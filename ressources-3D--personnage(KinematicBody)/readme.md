Ce répertoire contient les ressources utilisant le KINEMATIC BODY   (à ne pas confondre avec le Rigid Body).

le Kinematic Body ne respecte pas les lois de la physique, et est donc plus simple à utiliser.
(une autre répertoire contient les ressources pour le Rigid Body, qui respecte beaucoup mieux les lois de la physique, mais est parfois plus lourd à utiliser)
