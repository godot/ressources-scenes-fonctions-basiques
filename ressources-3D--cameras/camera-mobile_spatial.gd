extends Spatial

var speed = 200
var direction = Vector3()
# var gravity = -9.8
var gravity = 0

var velocity = Vector3()

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _physics_process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
	direction = Vector3 (0,0,0)
	if Input.is_action_pressed("camera_left"):
		direction.z -= 1
	if Input.is_action_pressed("camera_right"):
		direction.z += 1
	if Input.is_action_pressed("camera_forward"):
		direction.x += 1
	if Input.is_action_pressed("camera_backward"):
		direction.x -= 1
	if Input.is_action_pressed("camera_up"):
		direction.y += 1
	if Input.is_action_pressed("camera_down"):
		direction.y -= 1
	#orientation des mouvements à mettre sur axes locaux !!
	#rotation camera ci-dessous a completer
	# if Input.is_action_pressed("camera_rot-left"):
		#rotate_y( -= 1
	#if Input.is_action_pressed("camera_rot-right"):
		#rotation_degrees += 1
	#if Input.is_action_pressed("camera_rot-up"):
		#direction.y += 1
	#if Input.is_action_pressed("camera_rot-down"):
		#direction.y -= 1
	direction = direction.normalized()
	direction = direction * speed * delta
	
	velocity.y += gravity * delta
	velocity.x = direction.x
	velocity.z = direction.z
	
	#corriger avec les bonnes instructions pour spatial ou camera
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))










